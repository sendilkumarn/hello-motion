import { Component } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  keyframes
} from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('rotate', [
      transition(':enter', [
        style({transform: 'translateX(-100%)'}),
        animate(3000, keyframes([
          style({transform: 'rotate(0deg)'}),
          style({transform: 'rotate(180deg)'}),
          style({transform: 'rotate(360deg)'})
        ]))
      ]),
    ])
  ]
})
export class AppComponent {
  title = 'app';
}
